using ContactApi.Models;
using Microsoft.EntityFrameworkCore;

namespace ContactApi.Data
{
    public class ContactsApiDbContext: DbContext
    {
        public ContactsApiDbContext(DbContextOptions options): base(options)
        {
        }
        
        public virtual DbSet<Contact> Contacts { get; set; }
    }
}