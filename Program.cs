using ContactApi.Data;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// builder.Services.AddDbContext<ContactsApiDbContext>(options => options.UseInMemoryDatabase("ContactsDb"));
var serverVersion = new MySqlServerVersion(new Version(5, 7, 33));
builder.Services.AddDbContext<ContactsApiDbContext>(options => 
options.UseMySql(builder.Configuration.GetConnectionString("ContactsApiConnectionString"), serverVersion));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
