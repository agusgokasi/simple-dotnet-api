using ContactApi.Data;
using ContactApi.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace simple.Controllers;

[ApiController]
[Route("api/[controller]")]
public class ContactsController : ControllerBase
{
    private readonly ContactsApiDbContext dbContext;

    public ContactsController(ContactsApiDbContext dbContext)
    {
        this.dbContext = dbContext;
    }

    [HttpGet]
    public async Task<IActionResult> GetContacts()
    {
        return Ok(await dbContext.Contacts.ToListAsync());
    }

    [HttpPost]
    public async Task<IActionResult> AddContacts(AddContactRequest addContactRequest)
    {
        var contact = new Contact()
        {
            Id = Guid.NewGuid(),
            Address = addContactRequest.Address,
            Email = addContactRequest.Email,
            FullName = addContactRequest.FullName,
            Phone = addContactRequest.Phone
        };

        await dbContext.Contacts.AddAsync(contact);
        await dbContext.SaveChangesAsync();

        return Ok(contact);
    }

    [HttpGet]
    [Route("{id:guid}")]
    public async Task<IActionResult> GetContacts([FromRoute] Guid id)
    {
        var contact = await dbContext.Contacts.FindAsync(id);

        if (contact == null)
        {
            return NotFound();
        }

        return Ok(contact);
    }

    [HttpPut]
    [Route("{id:guid}")]
    public async Task<IActionResult> UpdateContacts([FromRoute] Guid id, UpdateContactRequest updateContactRequest)
    {
        var contact = await dbContext.Contacts.FindAsync(id);

        if (contact == null)
        {
            return NotFound();
        }

        contact.FullName = updateContactRequest.FullName;
        contact.Address = updateContactRequest.Address;
        contact.Phone = updateContactRequest.Phone;
        contact.Email = updateContactRequest.Email;

        await dbContext.SaveChangesAsync();

        return Ok(contact);
    }

    [HttpDelete]
    [Route("{id:guid}")]
    public async Task<IActionResult> DeleteContacts([FromRoute] Guid id)
    {
        var contact = await dbContext.Contacts.FindAsync(id);

        if (contact == null)
        {
            return NotFound();
        }

        dbContext.Remove(contact);
        await dbContext.SaveChangesAsync();

        return Ok(contact);
    }
}
